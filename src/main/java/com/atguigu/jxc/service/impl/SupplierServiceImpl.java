package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Override  //分页查询供应商
    public List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName) {
        //List<Supplier> supplierList = supplierDao.selectSupplierListByPage(page, rows, supplierName);
        return supplierDao.selectSupplierListByPage(page, rows, supplierName);
    }

    @Override  //供应商添加或修改 判断id有无,有则修改,无则添加
    @Transactional
    public int saveOrUpdate(Integer supplierId,Supplier supplier) {
        //判断,id为空,新增
        if (supplierId==null) {
            return supplierDao.saveSupplier(supplier);
        }else {  //不为空保存
            supplierDao.updateSupplier(supplierId,supplier);
            return supplierDao.saveSupplier(supplier);
        }
    }

    @Override   //删除供应商
    public void deleteSupplier(String[] ids) {
        supplierDao.deleteByIds(ids);
    }
}
