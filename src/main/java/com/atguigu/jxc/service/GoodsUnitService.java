package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface GoodsUnitService {

    //查询所有商品单位
    List<Unit> getUnitList();

}
