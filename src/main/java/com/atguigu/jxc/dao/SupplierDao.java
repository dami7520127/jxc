package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {

    //分页查询供应商
    List<Supplier> selectSupplierListByPage(@Param("page") Integer page, @Param("rows")Integer rows, @Param("supplierName")String supplierName);

    int saveSupplier(@Param("supplier") Supplier supplier);

    int updateSupplier(@Param("supplierId") Integer supplierId,@Param("supplier") Supplier supplier);

    void deleteByIds(@Param("supplierId") String[] ids);
}
