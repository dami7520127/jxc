package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsUnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.GoodsUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsUnitServiceImpl implements GoodsUnitService {

    @Autowired
    private GoodsUnitDao goodsUnitDao;

    @Override   //查询所有商品单位
    public List<Unit> getUnitList() {
        return goodsUnitDao.selectUnitList();
    }
}
