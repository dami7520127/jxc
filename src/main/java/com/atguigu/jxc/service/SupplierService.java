package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;

public interface SupplierService {

    //分页查询供应商
    List<Supplier> getSupplierList(Integer page, Integer rows, String supplierName);

    //供应商添加或修改 判断id有无,有则修改,无则添加
    int saveOrUpdate(Integer supplierId,Supplier supplier);

    //删除供应商
    void deleteSupplier(String[] ids);
}
