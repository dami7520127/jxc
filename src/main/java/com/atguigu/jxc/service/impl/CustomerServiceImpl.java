package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override   //客户列表模糊分页查询
    public List<Customer> getListByLike(Integer page, Integer rows, String customerName) {
        return customerDao.selectListByLike(page,rows,customerName);
    }

    @Override   //客户添加或修改
    public void customerSaveOrUpdate(Integer customerId, Customer customer) {
        if (customerId!=null){  //修改
            customerDao.customerUpdate(customerId,customer);
            //customerDao.customerSave(customer);
        }else {
            customerDao.customerSave(customer);
        }
    }

    @Override
    public void deleteCustomer(String[] ids) {
        customerDao.deleteCustomerByIds(ids);
    }
}
