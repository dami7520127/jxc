package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.GoodsUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/unit")
public class GoodsUnitController {

    @Autowired
    private GoodsUnitService goodsUnitService;

    //查询所有商品单位
    @PostMapping("/list")
    public Map<String,Object> getUnitList(){
        List<Unit> list = goodsUnitService.getUnitList();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
