package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/supplier")
@RestController
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    //分页查询供应商
    @PostMapping("/list")
    public Map<String, Object> getSupplierList(@RequestParam("page") Integer page,
                                               @RequestParam(value = "rows") Integer rows,
                                               @RequestParam(value = "supplierName", required = false) String supplierName) {
        page = page - 1;
        List<Supplier> list = supplierService.getSupplierList(page, rows, supplierName);
        int total = list.size();
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows", list);
        map.put("total", total);
        return map;
    }

    //供应商添加或修改 判断id有无,有则修改,无则添加
    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "supplierId", required = false) Integer supplierId, Supplier supplier) {
        int result;
        result = supplierService.saveOrUpdate(supplierId, supplier);
        if (result==1) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return null;
    }

    //删除供应商
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String[] ids){
        supplierService.deleteSupplier(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
