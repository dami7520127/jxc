package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/customer")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    //客户列表模糊分页查询
    @PostMapping("/list")
    public Map<String, Object> getListByLike(@RequestParam("page") Integer page,
                                             @RequestParam("rows") Integer rows,
                                             @RequestParam(value = "customerName", required = false) String customerName) {
        page = page - 1;
        HashMap<String, Object> map = new HashMap<>();
        List<Customer> list=customerService.getListByLike(page,rows,customerName);
        int total = list.size();
        map.put("total",total);
        map.put("rows",list);
        return map;
    }

    //客户添加或修改
    @PostMapping("/save")
    public ServiceVO customerSaveOrUpdate(@RequestParam(value = "customerId",required = false) Integer customerId, Customer customer){
        customerService.customerSaveOrUpdate(customerId,customer);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    //删除供客户
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String[] ids){
        customerService.deleteCustomer(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
