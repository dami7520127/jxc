package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

public interface GoodsUnitDao {
    List<Unit> selectUnitList();

}
