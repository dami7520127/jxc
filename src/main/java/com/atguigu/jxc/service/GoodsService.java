package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.List;

public interface GoodsService {


    ServiceVO getCode();

    //分页查询库存
    List<Goods> getGoodsStockList(Integer rows, Integer page, String codeOrName, Integer goodsTypeId);

    //分页查询商品信息
    List<Goods> getAllGoodsList(Integer rows, Integer page, String goodsName, Integer goodsTypeId);
}
