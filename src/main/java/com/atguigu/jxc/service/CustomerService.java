package com.atguigu.jxc.service;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public interface CustomerService {

    //客户列表模糊分页查询
    List<Customer> getListByLike(Integer page, Integer rows, String customerName);

    //客户添加或修改
    void customerSaveOrUpdate(Integer customerId, Customer customer);

    //删除
    void deleteCustomer(String[] ids);
}
