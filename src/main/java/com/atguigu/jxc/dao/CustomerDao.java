package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {

    //客户列表模糊分页查询
    List<Customer> selectListByLike(@Param("rows") Integer rows, @Param("page")Integer page, @Param("customerName") String customerName);

    //客户修改
    void customerUpdate(@Param("customerId")Integer customerId, @Param("customer")Customer customer);

    //客户添加
    void customerSave(@Param("customer")Customer customer);

    //删除
    void deleteCustomerByIds(@Param("customerId") String[] ids);
}
